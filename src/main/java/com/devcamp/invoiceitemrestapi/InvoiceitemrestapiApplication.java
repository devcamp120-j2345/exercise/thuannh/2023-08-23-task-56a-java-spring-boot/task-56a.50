package com.devcamp.invoiceitemrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceitemrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceitemrestapiApplication.class, args);
	}

}
